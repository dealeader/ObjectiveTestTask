using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using ObjectiveTestTask.Persistence;

namespace ObjectiveTestTask.Infrastructure;

public class ApartmentsExternalService
{
    private readonly ApartmentsDb _db;
    private readonly PrinzipApiService _api;
    private readonly EmailService _emailService;
    private readonly ILogger<ApartmentsExternalService> _logger;

    public ApartmentsExternalService(IDbContextFactory<ApartmentsDb> db, ILogger<ApartmentsExternalService> logger, IServiceProvider sp)
    {
        _logger = logger;
        _emailService = new EmailService(sp.GetRequiredService<ILogger<EmailService>>(), sp.GetRequiredService<IOptions<EmailServiceOptions>>());
        _db = db.CreateDbContext();
        _api = new PrinzipApiService(new HttpClient(), sp.GetRequiredService<IOptions<PrinzipApiOptions>>());
    }

    public async Task Fetch(CancellationToken token)
    {
        var dbResult = await _db.Apartments.ToArrayAsync(cancellationToken: token);
        if (dbResult.Length == 0) return;
                
        _logger.LogInformation("Price data update launched");
        foreach (var apartment in dbResult)
        {
            var apartmentUri = new Uri(apartment.Url);
            var prinzipApartment = await _api.GetApartmentPriceByUrl($"{apartmentUri.AbsolutePath}");
            
            // добавить в условие для хард-теста && !apartmentUri.Segments.Contains("61892")
            if (prinzipApartment.Price.Equals(apartment.Price))
                continue;
            
            apartment.Price = prinzipApartment.Price;

            // хард-тест для сценария обновления цены
            // if (apartmentUri.Segments.Contains("61892"))
            //     apartment.Price = new Random().Next(100000, 10_000_000).ToString();

            _db.Apartments.Update(apartment);
            await _db.SaveChangesAsync(token);

            var apartmentSubscription = await _db.Subscriptions.FindAsync(new object?[] { apartment.SubscriptionId }, cancellationToken: token) 
                                        ?? throw new ArgumentNullException();
            await _emailService.SendApartmentPriceUpdateEmailAsync(apartmentSubscription.Email, apartment.Url, apartment.Price);
        }
    }
}