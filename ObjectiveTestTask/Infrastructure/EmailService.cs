using Microsoft.Extensions.Options;
using MimeKit;

namespace ObjectiveTestTask.Infrastructure;

public class EmailService
{
    private readonly ILogger<EmailService> _logger;
    private readonly EmailServiceOptions _options;

    public EmailService(ILogger<EmailService> logger, IOptions<EmailServiceOptions> options)
    {
        _logger = logger;
        _options = options.Value;
    }

    public async Task SendSubscriptionEmailAsync(string email, string uri)
    {
        using var message = new MimeMessage();
        
        message.From.Add(new MailboxAddress("Подписка на обновление цены квартиры", _options.Username));
        message.To.Add(new MailboxAddress("", email));
        message.Subject = "Подписка на обновление цены квартиры";
        message.Body = new TextPart("Plain")
        {
            Text = $"Вы подписались на обновление цены квартиры по адресу {uri}"
        };

        using var client = new MailKit.Net.Smtp.SmtpClient();
        
        await client.ConnectAsync(_options.Host, _options.Port);
        await client.AuthenticateAsync(_options.Username, _options.Password);
        await client.SendAsync(message);
        await client.DisconnectAsync(true);
        
        _logger.LogInformation("New subscription email to {email} is sent", email);
    }

    public async Task SendApartmentPriceUpdateEmailAsync(string email, string uri, string price)
    {
        using var message = new MimeMessage();
        
        message.From.Add(new MailboxAddress("Подписка на обновление цены квартиры", "fake@yandex.ru"));
        message.To.Add(new MailboxAddress("", email));
        message.Subject = "Обновлена цена на квартиру";
        message.Body = new TextPart("Plain")
        {
            Text = $"Цена квартиры по адресу {uri} изменилась, новая цена: {price}"
        };

        using var client = new MailKit.Net.Smtp.SmtpClient();
        
        await client.ConnectAsync(_options.Host, _options.Port);
        await client.AuthenticateAsync(_options.Username, _options.Password);
        await client.SendAsync(message);
        await client.DisconnectAsync(true);
        
        _logger.LogInformation("New price update email to {email} is sent", email);
    }

    private static async void UseSmptClient(MimeMessage message, EmailServiceOptions options)
    {
        using var client = new MailKit.Net.Smtp.SmtpClient();
        
        await client.ConnectAsync(options.Host, options.Port);
        await client.AuthenticateAsync(options.Username, options.Password);
        await client.SendAsync(message);
        await client.DisconnectAsync(true);
    }
}