namespace ObjectiveTestTask.Infrastructure;

public class PrinzipApiOptions
{
    public string? Host { get; set; }
    public string[]? EstateTypes { get; set; }
    public string[]? Districts { get; set; }
}