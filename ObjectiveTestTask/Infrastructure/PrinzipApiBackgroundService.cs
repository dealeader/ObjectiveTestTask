namespace ObjectiveTestTask.Infrastructure;

public class PrinzipApiBackgroundService : BackgroundService
{
    private readonly ApartmentsExternalService _service;
    
    public PrinzipApiBackgroundService(ApartmentsExternalService service)
    {
        _service = service;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        while (!stoppingToken.IsCancellationRequested)
        {
            try
            {
                await _service.Fetch(stoppingToken);
            }
            catch (Exception ex)
            {
                throw;
            }

            await Task.Delay(60000, stoppingToken);
        }
    }
}