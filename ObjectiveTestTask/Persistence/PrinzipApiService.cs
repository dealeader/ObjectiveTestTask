using Microsoft.Extensions.Options;
using ObjectiveTestTask.Infrastructure;

namespace ObjectiveTestTask.Persistence;

public class PrinzipApiService
{
    private readonly HttpClient _httpClient;

    public PrinzipApiService(HttpClient httpClient, IOptions<PrinzipApiOptions> options)
    {
        _httpClient = httpClient;
        _httpClient.BaseAddress = new Uri($"https://{options.Value.Host}");
        
        // _httpClient.DefaultRequestHeaders.Add(
        //     HeaderNames.Accept, "application/json");
    }

    public async Task<PrinzipApiApartment> GetApartmentPriceByTokens(string district, string flatId)
        => await _httpClient.GetFromJsonAsync<PrinzipApiApartment>($"/apartments/{district}/{flatId}?ajax=1") ?? 
           throw new InvalidOperationException();

    public async Task<PrinzipApiApartment> GetApartmentPriceByUrl(string url)
        => await _httpClient.GetFromJsonAsync<PrinzipApiApartment>($"{url}?ajax=1") ??
           throw new InvalidOperationException();
}