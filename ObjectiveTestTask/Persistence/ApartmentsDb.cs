using Microsoft.EntityFrameworkCore;
using ObjectiveTestTask.Domain;

namespace ObjectiveTestTask.Persistence;

public sealed class ApartmentsDb : DbContext
{
    public ApartmentsDb(DbContextOptions<ApartmentsDb> options) : base(options)
    {
        Database.Migrate();
    }

    public DbSet<Apartment> Apartments => Set<Apartment>();
    public DbSet<Subscription> Subscriptions => Set<Subscription>();
}