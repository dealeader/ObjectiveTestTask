using ObjectiveTestTask.Domain.Services;

namespace ObjectiveTestTask.Domain.Features;

public class GetApartmentsSubsribesCommand
{
    private readonly ApartmentsService _apartmentsService;

    public GetApartmentsSubsribesCommand(ApartmentsService apartmentsService)
    {
        _apartmentsService = apartmentsService;
    }

    public async Task<Apartment[]> ExecuteAsync()
    {
        var result = await _apartmentsService.GetSubsribes();
        return result;
    }
}