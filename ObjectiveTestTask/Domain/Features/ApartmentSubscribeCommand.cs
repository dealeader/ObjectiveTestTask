using Microsoft.Extensions.Options;
using ObjectiveTestTask.Api.Extensions;
using ObjectiveTestTask.Domain.Services;
using ObjectiveTestTask.Infrastructure;

namespace ObjectiveTestTask.Domain.Features;

public class ApartmentSubscribeCommand
{
    private readonly ApartmentsService _apartmentsService;
    private readonly PrinzipApiOptions _options;

    public ApartmentSubscribeCommand(ApartmentsService apartmentsService, IOptions<PrinzipApiOptions> options)
    {
        _apartmentsService = apartmentsService;
        _options = options.Value;
    }

    public async Task ExecuteAsync(Parameters parameters)
    {
        var uri = parameters.Uri;

        if (!(uri.IsCorrectHost(_options.Host) && uri.IsCorrectEstateTypeSegment(_options.EstateTypes)))
            throw new Exception();

        var districtSegment = uri.GetDistrictSegment(_options.Districts);
        var flatId = uri.GetFlatIdSegment();
        
        await _apartmentsService.Subscribe(parameters.Id,parameters.Email, uri.ToString(), districtSegment, flatId);
    }
    
    public record Parameters(Guid Id, Uri Uri, string Email);
}