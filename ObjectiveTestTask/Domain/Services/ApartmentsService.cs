using Microsoft.EntityFrameworkCore;
using ObjectiveTestTask.Infrastructure;
using ObjectiveTestTask.Persistence;

namespace ObjectiveTestTask.Domain.Services;

public class ApartmentsService
{
    private readonly ApartmentsDb _db;
    private readonly PrinzipApiService _prinzipApiService;
    private readonly EmailService _emailService;

    public ApartmentsService(ApartmentsDb db, PrinzipApiService prinzipApiService, EmailService emailService)
    {
        _db = db;
        _prinzipApiService = prinzipApiService;
        _emailService = emailService;
    }

    public async Task Subscribe(Guid id, string email, string uri, string district, string flatId)
    {
        var prinzipApiResult = await _prinzipApiService.GetApartmentPriceByTokens(district, flatId);

        var subscription = await _db.Subscriptions.Include(s => s.Apartments)
            .FirstOrDefaultAsync(s => s.Email.Equals(email));

        if (subscription is null)
        {
            var newSubscription = new Subscription(Guid.NewGuid(), email);
            await _db.Subscriptions.AddAsync(newSubscription);
            subscription = newSubscription;
        }

        if (!subscription.Apartments.Any(a => a.Url.Equals(uri)))
        {
            await _db.Apartments.AddAsync(new Apartment(id, prinzipApiResult.Price, uri, subscription.Id));
            await _db.SaveChangesAsync();
            await _emailService.SendSubscriptionEmailAsync(email, uri);
        }
        
        await _db.SaveChangesAsync();
    }

    public async Task<Apartment[]> GetSubsribes()
    {
        var dbResult = await _db.Apartments.ToArrayAsync();

        return dbResult;
    }

    public async Task RefreshApartmentsPrices()
    {
        var dbResult = await _db.Apartments.ToArrayAsync();
        if (dbResult.Length == 0) return;

        foreach (var apartment in dbResult)
        {
            var apartmentUri = new Uri(apartment.Url);
            var prinzipApartment = await _prinzipApiService.GetApartmentPriceByUrl($"{apartmentUri.AbsolutePath}");

            if (prinzipApartment.Price.Equals(apartment.Price))
                continue;

            apartment.Price = prinzipApartment.Price;

            _db.Apartments.Update(apartment);
            await _db.SaveChangesAsync();
        }
    }
}