namespace ObjectiveTestTask.Domain;

public class Subscription
{
    public Subscription(Guid id, string email)
    {
        Id = id;
        Email = email;
    }

    public Guid Id { get; set; }
    public string Email { get; set; }
    public ICollection<Apartment> Apartments { get; set; } = new List<Apartment>();
}