namespace ObjectiveTestTask.Domain;

public class Apartment
{
    public Apartment(Guid id, string price, string url, Guid subscriptionId)
    {
        Id = id;
        Price = price;
        Url = url;
        SubscriptionId = subscriptionId;
    }

    public Guid Id { get; set; }
    public string Price { get; set; }
    public string Url { get; set; }
    public Guid SubscriptionId { get; set; }
}