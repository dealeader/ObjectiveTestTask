using Microsoft.EntityFrameworkCore;
using ObjectiveTestTask.Api.Extensions;
using ObjectiveTestTask.Domain.Features;
using ObjectiveTestTask.Domain.Services;
using ObjectiveTestTask.Infrastructure;
using ObjectiveTestTask.Persistence;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddDbContextPool<ApartmentsDb>(opt 
    => opt.UseSqlite(builder.Configuration.GetConnectionString("Sqlite")));
builder.Services.AddPooledDbContextFactory<ApartmentsDb>(o 
    => o.UseSqlite(builder.Configuration.GetConnectionString("Sqlite")));

builder.Services.AddOptions<PrinzipApiOptions>().BindConfiguration(nameof(PrinzipApiOptions));

builder.Services.AddOptions<EmailServiceOptions>().BindConfiguration(nameof(EmailServiceOptions));

builder.Services.AddHttpClient<PrinzipApiService>();

builder.Services.AddSingleton<ApartmentsExternalService>();

builder.Services.AddScoped<EmailService>();
builder.Services.AddScoped<ApartmentsService>();
builder.Services.AddScoped<ApartmentSubscribeCommand>();
builder.Services.AddScoped<ApartmentSubscribeCommandAdapter>();
builder.Services.AddScoped<GetApartmentsSubsribesCommand>();
builder.Services.AddScoped<GetApartmentsSubscribesQueryAdapter>();

builder.Services.AddHostedService<PrinzipApiBackgroundService>();

builder.Services.AddControllers();

var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI(c => 
{ 
    c.SwaggerEndpoint("/swagger/v1/swagger.json", "ObjectiveTestTask");
});

app.MapControllers();
app.UseHttpsRedirection();
app.Run();