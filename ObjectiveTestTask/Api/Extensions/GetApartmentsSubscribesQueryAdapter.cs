using ObjectiveTestTask.Api.Models;
using ObjectiveTestTask.Domain.Features;

namespace ObjectiveTestTask.Api.Extensions;

public class GetApartmentsSubscribesQueryAdapter
{
    private readonly GetApartmentsSubsribesCommand _command;

    public GetApartmentsSubscribesQueryAdapter(GetApartmentsSubsribesCommand command)
    {
        _command = command;
    }

    public async Task<ApartmentsResponse> Execute()
    {
        var response = await _command.ExecuteAsync();
        
        return new ApartmentsResponse()
        {
            Apartments = response
        };
    }
}