using ObjectiveTestTask.Api.Models;
using ObjectiveTestTask.Domain.Features;

namespace ObjectiveTestTask.Api.Extensions;

public class ApartmentSubscribeCommandAdapter
{
    private readonly ApartmentSubscribeCommand _command;

    public ApartmentSubscribeCommandAdapter(ApartmentSubscribeCommand command)
    {
        _command = command;
    }

    public async Task<SubscribeApartmentResponse> Execute(ApartmentSubscriptionRequestBody body)
    {
        var parameters = Convert(body);
        await _command.ExecuteAsync(parameters);
        return new SubscribeApartmentResponse(parameters.Id, body.Url, body.Email);
    }

    private static ApartmentSubscribeCommand.Parameters Convert(ApartmentSubscriptionRequestBody body)
        => new(Guid.NewGuid(),new Uri(body.Url), body.Email);
}