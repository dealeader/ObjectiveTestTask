namespace ObjectiveTestTask.Api.Extensions;

public static class UriExtensions
{

    private static readonly string[] PrinzipEstateTypeSegments = new[] { "apartments" };

    private static readonly string[] PrinzipDistrictSegments = new[] { "newtonpark", "l8" };

    public static bool IsCorrectHost(this Uri uri, string? host) => uri.Host.Equals(host);

    public static bool IsCorrectEstateTypeSegment(this Uri uri, IEnumerable<string>? tokens)
        => tokens != null && IsCorrectSegment(uri, tokens.Contains);

    private static bool IsCorrectDistrictSegment(this Uri uri, IEnumerable<string>? tokens)
        => tokens != null && IsCorrectSegment(uri, tokens.Contains);

    private static bool IsCorrectFlatIdSegment(this Uri uri)
        => IsCorrectSegment(uri,  s 
            =>
                {
                    var value = int.TryParse(s, out var parsed);
                    return value;
                });

    private static bool IsCorrectSegment(this Uri uri,  Func<string, bool> predicate)
    {
        var segment = uri.Segments
            .Select(s => s.ReplaceSlash())
            .SingleOrDefault(predicate);
        
        return segment is not null;
    }

    public static string GetDistrictSegment(this Uri uri, string[]? tokens)
    {
        if (!uri.IsCorrectDistrictSegment(tokens))
            throw new ArgumentException();
        return uri.Segments.First(s => tokens != null && tokens.Contains(s.ReplaceSlash())).ReplaceSlash();
    } 
    
    public static string GetFlatIdSegment(this Uri uri)
    {
        if (!uri.IsCorrectFlatIdSegment())
            throw new ArgumentException();
        return uri.Segments.Last().ReplaceSlash();
    }
    
    private static string ReplaceSlash(this string s)
    {
        return s.Replace("/", "");
    }
}