using System.ComponentModel.DataAnnotations;

namespace ObjectiveTestTask.Api.Models;

public class ApartmentSubscriptionRequestBody
{
    [Required][Url] public string Url { get; set; }
    [Required][EmailAddress] public string Email { get; set; }
}