using ObjectiveTestTask.Domain;

namespace ObjectiveTestTask.Api.Models;

public class ApartmentsResponse
{
    public Apartment[] Apartments { get; set; }
}