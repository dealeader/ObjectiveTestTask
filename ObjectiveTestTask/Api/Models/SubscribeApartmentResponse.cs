namespace ObjectiveTestTask.Api.Models;

public class SubscribeApartmentResponse
{
    public SubscribeApartmentResponse(Guid id, string url, string email)
    {
        Url = url;
        Email = email;
        Id = id;
    }

    public Guid Id { get; set; }
    public string Url { get; set; }
    public string Email { get; set; }
}