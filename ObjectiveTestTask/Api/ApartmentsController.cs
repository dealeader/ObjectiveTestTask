using Microsoft.AspNetCore.Mvc;
using ObjectiveTestTask.Api.Extensions;
using ObjectiveTestTask.Api.Models;

namespace ObjectiveTestTask.Api;

[ApiController]
[Route("api/[controller]")]
public class ApartmentsController : ControllerBase
{
    private readonly ApartmentSubscribeCommandAdapter _apartmentSubscribeCommandAdapter;
    private readonly GetApartmentsSubscribesQueryAdapter _getApartmentsSubscribesQueryAdapter;
    private readonly IConfiguration _configuration;

    public ApartmentsController(ApartmentSubscribeCommandAdapter apartmentSubscribeCommandAdapter, 
        GetApartmentsSubscribesQueryAdapter getApartmentsSubscribesQueryAdapter, IConfiguration configuration)
    {
        _apartmentSubscribeCommandAdapter = apartmentSubscribeCommandAdapter;
        _getApartmentsSubscribesQueryAdapter = getApartmentsSubscribesQueryAdapter;
        _configuration = configuration;
    }

    [HttpPost]
    public async Task<IActionResult> Subscribe([FromBody] ApartmentSubscriptionRequestBody body)
    {
        var response = await _apartmentSubscribeCommandAdapter.Execute(body);
        
        return Ok(response);
    }

    [HttpGet]
    public async Task<IActionResult> GetApartments()
    {
        var response = await _getApartmentsSubscribesQueryAdapter.Execute();
        
        return Ok(response);
    }
}